const config = require("../config");
const express = require("express");
const app = express();

// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// Simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Api Paypal." });
});

// Set port, listen for requests
const PORT = process.env.PORT || config.paypal.PORT;
app.listen(PORT, () => {
  console.log(`Server Api paypal is running on port ${PORT}.`);
});
