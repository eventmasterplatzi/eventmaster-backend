# Our Project EventMaster
The project **eventMaster** we built it with an infrastructure of microservices (auth, store, chat, Paypal and doc).  We start with an Express web server as a base for all our microservices. Next, we add configuration for MySQL database, create the models with Sequelize ORM, write the controllers. Then we define routes for handling all CRUD operations (including custom finder in order to our necessaries). Some of the technologies or tools and packages of our stack were the follows: Node.js, express, MySQL, sequelize, socket.io, multer, swagger, jsonwebtoken, Paypal SDK, Postman, PM2, Digital Ocean.

## Rest CRUD API overview
We build Rest Apis that can create, retrieve, update, delete, and find records having some fields key. These Apis corresponding to each element of our database model. Then, we have endpoints to the features most important of the project EventMaster.

## Project structure

![Project sctruture](/resources/screenshots/project_structure.jpg)

## Finally, we did a test for all Rest Apis using Postman. We will show the tests most important realized.
### SingUp User
![Signup](./resources/screenshots/signup.jpg)

### Singin User with JWT
![Signup](./resources/screenshots/jwt.jpg)

### Register an event
![Signup](./resources/screenshots/event.jpg)

### Deploy - services running (not all)
![Signup](/resources/screenshots/deploy.jpg)


## Now, you can explore this project only git clone the repository and install it

### Clone it

```bash
git clone https://gitlab.com/eventmasterplatzi/eventmaster-backend.git 
or
git clone git@gitlab.com:eventmasterplatzi/eventmaster-backend.git

```

### Install it

```bash

npm install

```

### Our configuration file can do it like this:

```js
module.exports = {
  auth: {
    PORT: 3001,
    SECRET: "your-key-secret",
  },
  store: {
    PORT: 3002,
    HOST: "localhost",
    USER: "user_database",
    PASSWORD: "user_password",
    DB: "evenmaster",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
  chat: {
    PORT: 3003,
  },
  paypal: {
    PORT: 3004,
  },
  doc: {
    PORT: 3005,
  },
};


````


