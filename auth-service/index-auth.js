const config = require("../config");
const express = require("express");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "http://localhost:8081",
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Api Rest Auth woth Json Web Token." });
});

// routes
require("./routes/auth.routes")(app);
require("./routes/user.routes")(app);

// Set port, listen for requests
const PORT = process.env.PORT || config.auth.PORT;
app.listen(PORT, () => {
  console.log(`Server Api Rest Auth is running on port ${PORT}.`);
});
