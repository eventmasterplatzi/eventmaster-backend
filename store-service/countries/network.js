const countries = require('./country.controller.js');

const router = require('express').Router();

// Create a new Country
router.post('/', countries.create);

// Retrieve all Countries
router.get('/', countries.findAll);

// Retrieve a single Countries with id
router.get('/:id', countries.findOne);

// Update a Countries with id
router.put('/:id', countries.update);

// Delete a Countries with id
router.delete('/:id', countries.delete);

// Create a new Countries
router.delete('/', countries.deleteAll);

// Insert multiple Countries
router.post('/all', countries.insertAll);

module.exports = router;
