const cities = require('./city.controller.js');

const router = require('express').Router();

// Create a new City
router.post('/', cities.create);

// Retrieve all Cities
router.get('/', cities.findAll);

// Retrieve all Cities per Country
router.get('/country', cities.findCitiesCountry);

// Retrieve a single Cities with id
router.get('/:id', cities.findOne);

// Update a Cities with id
router.put('/:id', cities.update);

// Delete a Cities with id
router.delete('/:id', cities.delete);

// Create a new Cities
router.delete('/', cities.deleteAll);

// Insert multiple Cities
router.post('/all', cities.insertAll);

module.exports = router;
