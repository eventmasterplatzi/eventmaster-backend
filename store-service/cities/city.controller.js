const db = require('../models');

const City = db.cities;
const { Op } = db.Sequelize;

// Create and Save a new City
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({
      message: 'Content can not be empty!',
    });
    return;
  }

  // Create a City
  const city = {
    countryCode: req.body.country,
    name: req.body.name,
    lat: req.body.lat,
    lng: req.body.lng,
  };

  // Save City in the database
  City.create(city)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the city.',
      });
    });
};

// Retrieve all Cities from the database.
exports.findAll = (req, res) => {
  const { name } = req.query;
  const condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

  City.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving cities.',
      });
    });
};

//Retrieve cities per country
exports.findCitiesCountry = (req, res) => {
  const { countryCode } = req.query;
  const condition = countryCode
    ? { countryCode: { [Op.like]: `%${countryCode}%` } }
    : null;

  City.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving cities.',
      });
    });
};

// Find a single City with an id
exports.findOne = (req, res) => {
  const { id } = req.params;

  City.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error retrieving city with id=${id}`,
      });
    });
};

// Update a City by the id in the request
exports.update = (req, res) => {
  const { id } = req.params;

  City.update(req.body, {
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'City was updated successfully.',
        });
      } else {
        res.send({
          message: `Cannot update city with id=${id}. Maybe city was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error updating city with id=${id}`,
      });
    });
};

// Delete a City with the specified id in the request
exports.delete = (req, res) => {
  const { id } = req.params;

  City.destroy({
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'City was deleted successfully!',
        });
      } else {
        res.send({
          message: `Cannot delete city with id=${id}. Maybe city was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Could not delete city with id=${id}`,
      });
    });
};

// Delete all Cities from the database.
exports.deleteAll = (req, res) => {
  City.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} cities were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while removing all cities.',
      });
    });
};

exports.insertAll = (req, res) => {
  City.bulkCreate(req.body, { individualHooks: false })
    .then(function (response) {
      res.json(response);
    })
    .catch(function (error) {
      res.json(error);
    });
};
