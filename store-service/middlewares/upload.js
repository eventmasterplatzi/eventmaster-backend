const multer = require('multer');

const imageFilter = (req, file, callback) => {
  if (file.mimetype.startsWith('image')) {
    callback(null, true);
  } else {
    callback('Please upload only images.', false);
  }
};

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, __basedir + '/../resources/assets/uploads/');
  },
  filename: (req, file, callback) => {
    callback(null, `${Date.now()}_${file.originalname}`);
  },
});

const uploadFile = multer({ storage: storage, fileFilter: imageFilter });
module.exports = uploadFile;
