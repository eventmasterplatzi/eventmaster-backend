const router = require('express').Router();
const uploadController = require('./image.controller.js');
const upload = require('../middlewares/upload');

router.post('/upload', upload.single('file'), uploadController.uploadFiles);
// Retrieve all Images
router.get('/', uploadController.findAll);

module.exports = router;
