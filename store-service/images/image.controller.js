const fs = require('fs');
const path = require('path');
const db = require('../models');
const Image = db.images;

exports.uploadFiles = async (req, res) => {
  try {
    console.log(req.file);

    if (req.file == undefined) {
      return res.send({
        message: `You must select a file.`,
      });
    }

    Image.create({
      type: req.file.mimetype,
      name: req.file.originalname,
      userId: req.body.userId,
      category: req.body.category,
    }).then((image) => {
      const extension = image.name.split('.');
      let filePath =
        __basedir + `/../resources/assets/${image.userId}/${image.category}/`;
      if (!fs.existsSync(filePath)) {
        fs.mkdirSync(filePath, { recursive: true });
      }
      const newFile = `${filePath}${image.id}` + '.' + extension[1];
      fs.writeFileSync(
        newFile,
        fs.readFileSync(
          __basedir + '/../resources/assets/uploads/' + req.file.filename
        )
      );
      req.body.name = `${image.id}` + '.' + extension[1];
      Image.update(req.body, {
        where: { id: image.id },
      });

      return res.send({
        id: image.id,
        path: newFile,
        message: `File has been uploaded successfully`,
      });
    });
  } catch (error) {
    console.log(error);
    return res.send({
      message: `Error when trying upload images: ${error}`,
    });
  }
};

// Retrieve all Images from the database.
exports.findAll = (req, res) => {
  const { userId } = req.query;
  const condition = userId ? { userId: userId } : null;

  Image.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving images.',
      });
    });
};
