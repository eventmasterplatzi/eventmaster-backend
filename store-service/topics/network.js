const topics = require('./topic.controller.js');

var router = require('express').Router();

// Create a new Topic
router.post('/', topics.create);

// Retrieve all Topics
router.get('/', topics.findAll);

// Retrieve all Topics per user
router.get('/user/:id', topics.findAllUser);

// Retrieve all status Topics
router.get('/status', topics.findAllStatus);

// Retrieve a single Topics with id
router.get('/:id', topics.findOne);

// Update a Topics with id
router.put('/:id', topics.update);

// Delete a Topics with id
router.delete('/:id', topics.delete);

// Create a new Topics
router.delete('/', topics.deleteAll);

module.exports = router;
