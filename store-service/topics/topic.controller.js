const db = require('../models');
const Topic = db.topics;
const Category = db.categories;
const Op = db.Sequelize.Op;

// Create and Save a new Topic
exports.create = (req, res) => {
  if (req.body.categoryId) {
    Category.findByPk(req.body.categoryId)
      .then((data) => {
        if (data) {
          console.log(data);
        } else {
          res.status(201).send({
            message:
              'There is no category with identified as ' + req.body.categoryId,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: 'Error retrieving category with id=' + err,
        });
      });
  } else {
    res.send({ message: 'There is no category in the record.' });
    return;
  }

  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: 'Content can not be empty!',
    });
    return;
  }

  // Create a Topic
  const topic = {
    title: req.body.title,
    description: req.body.description,
    status: req.body.status ? req.body.status : false,
    owner: req.body.owner ? req.body.owner : 1,
    categoryId: req.body.categoryId ? req.body.categoryId : 0,
  };

  // Save Topic in the database
  Topic.create(topic)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the topic.',
      });
    });
};

// Retrieve all Topics from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Topic.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving topics.',
      });
    });
};

// Retrieve all Topics from the database per user.
exports.findAllUser = (req, res) => {
  const { id } = req.params;
  const condition = id ? { owner: [id, 1] } : null;

  Topic.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving topics.',
      });
    });
};

// Find a single Topic with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Topic.findByPk(id)
    .then((data) => {
      if (data) {
        res.send(data);
      } else {
        res.send({
          message: 'there are no topics for this query ID:' + id,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Error retrieving topic with id=' + id,
      });
    });
};

// Update a Topic by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Topic.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'Topic was updated successfully.',
        });
      } else {
        res.send({
          message: `Cannot update topic with id=${id}. Maybe topic was not found or is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Error updating topic with id=' + id,
      });
    });
};

// Delete a Topic with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Topic.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'Topic was deleted successfully!',
        });
      } else {
        res.send({
          message: `Cannot delete topic with id=${id}. Maybe topic was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Could not delete topic with id=' + id,
      });
    });
};

// Delete all Topics from the database.
exports.deleteAll = (req, res) => {
  Topic.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} topics were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while removing all topics.',
      });
    });
};

// Find all published Topics
exports.findAllStatus = (req, res) => {
  Topic.findAll({ where: { status: true } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving topics.',
      });
    });
};
