const db = require('../models');

const User = db.users;
const UserCategories = db.user_categories;
const UserTopics = db.user_topics;
const { Op } = db.Sequelize;

// Create and Save a new User
exports.create = (req, res) => {
  // Validate request
  let types = User.rawAttributes.type.values;
  if (!req.body.email) {
    res.status(400).send({
      message: 'Content can not be empty!',
    });
    return;
  }
  if (types.includes(req.body.type) == false || !req.body.type) {
    res.status(400).send({
      message: 'Type User can not be empty!',
    });
    return;
  }
  // Create a User
  const user = {
    name: req.body.name,
    email: req.body.email,
    biography: req.body.biography,
    avatar: req.body.avatar ? req.body.avatar : null,
    type: req.body.type,
    status: req.body.status ? req.body.status : 1,
  };

  // Save User in the database
  User.create(user)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.errors[0].message ||
          'Some error occurred while creating the user.',
      });
    });
};

// Retrieve all Users from the database.
exports.findAll = (req, res) => {
  const { type } = req.query;
  const condition = type ? { type: { [Op.like]: `%${type}%` } } : null;

  User.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving users.',
      });
    });
};

// Find a single User with an email
exports.findOne = (req, res) => {
  const { id } = req.params;
  var categories = [];
  var topics = [];
  var obj_data_user,
    obj_json = [];
  User.findByPk(id)
    .then((data) => {
      obj_data_user = data;
      //Get user categories
      db.sequelize
        .query('SELECT * FROM user_categories where userId=' + id, null, {
          raw: true,
          plain: false,
        })
        .then(function (category) {
          for (let j = 0; j < category[0].length; j++) {
            categories.push(category[0][j].categoryId);
          }
          console.log(categories);
        });
      //Get user topics
      db.sequelize
        .query('SELECT * FROM user_topics where userId=' + id, null, {
          raw: true,
          plain: false,
        })
        .then(function (topic) {
          for (let h = 0; h < topic[0].length; h++) {
            topics.push(topic[0][h].topicId);
          }
          console.log(topics);
          obj_data_user = JSON.stringify(obj_data_user);
          obj_json = JSON.parse(obj_data_user);
          obj_json.categories = categories;
          obj_json.topics = topics;

          res.send(obj_json);
        });
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error retrieving user with id=${id}`,
      });
    });
};

// Find a single User with an email
exports.findOneEmail = (req, res) => {
  const { email } = req.body;
  User.findAll({ where: { email: email } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving users.',
      });
    });
};

// Update a User by the id in the request
exports.update = (req, res) => {
  const { id } = req.params;

  User.update(req.body, {
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        if (req.body.categories) {
          for (let i = 0; i < req.body.categories.length; i++) {
            UserCategories.findOrCreate({
              where: {
                categoryId: req.body.categories[i],
                userId: id,
              },
            }).spread(function (cat, created) {
              if (created) {
                console.log('New category add ' + req.body.categories[i]);
              } else {
                //Register user preferred categories
                if (!req.body.categories.includes(req.body.categories[i])) {
                  db.sequelize
                    .query(
                      'DELETE FROM user_categories where userId=' +
                        id +
                        ' AND categoryId=' +
                        req.body.categories[i],
                      null,
                      {
                        raw: true,
                      }
                    )
                    .then(function (category) {
                      console.log('Deleted category' + req.body.categories[i]);
                    });
                } else {
                  console.log(
                    'This Category ' + req.body.categories[i] + ' stay here'
                  );
                }
              }
            });
          }
          UserCategories.destroy({
            where: {
              categoryId: { [Op.notIn]: req.body.categories },
              userId: id,
            },
          }).then((num) => {
            if (num == 1) {
              console.log('Some categories was deleted successfully!');
            } else {
              console.log('Any category was deleted!');
            }
          });
        }
        //Register user preferred topics
        if (req.body.topics) {
          for (let i = 0; i < req.body.topics.length; i++) {
            UserTopics.findOrCreate({
              where: {
                topicId: req.body.topics[i],
                userId: id,
              },
            }).spread(function (top, created) {
              if (created) {
                console.log('New topic add ' + req.body.topics[i]);
              } else {
                //Get user topics
                if (!req.body.topics.includes(req.body.topics[i])) {
                  db.sequelize
                    .query(
                      'DELETE FROM user_topics where userId=' +
                        id +
                        ' AND topicId=' +
                        req.body.topics[i],
                      null,
                      {
                        raw: true,
                      }
                    )
                    .then(function (topic) {
                      console.log('Deleted topic' + req.body.topics[i]);
                    });
                } else {
                  console.log(
                    'This Topic ' + req.body.topics[i] + ' stay here'
                  );
                }
              }
            });
          }
          UserTopics.destroy({
            where: {
              topicId: { [Op.notIn]: req.body.topics },
              userId: id,
            },
          }).then((num) => {
            if (num == 1) {
              console.log('Some topics was deleted successfully!');
            } else {
              console.log('Any topic was deleted!');
            }
          });
        }
        res.send({
          message: 'User was updated successfully.',
        });
      } else {
        res.send({
          message: `Cannot update user with id=${id}. Maybe user was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error updating user with id=${id}` + err,
      });
    });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
  const { id } = req.params;

  User.destroy({
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'User was deleted successfully!',
        });
      } else {
        res.send({
          message: `Cannot delete user with id=${id}. Maybe user was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Could not delete user with id=${id}`,
      });
    });
};

// Delete all Users from the database.
exports.deleteAll = (req, res) => {
  User.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} users were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while removing all users.',
      });
    });
};

// Find all status Users
exports.findAllStatus = (req, res) => {
  User.findAll({ where: { status: true } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving users.',
      });
    });
};
