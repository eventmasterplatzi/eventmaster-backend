const categories = require('./category.controller.js');

const router = require('express').Router();

// Create a new Category
router.post('/', categories.create);

// Retrieve all Categories
router.get('/', categories.findAll);

// Retrieve all Categories per User
router.get('/user/:id', categories.findAllUser);

// Retrieve all status Categories
router.get('/status', categories.findAllStatus);

// Retrieve a single Categories with id
router.get('/:id', categories.findOne);

// Update a Categories with id
router.put('/:id', categories.update);

// Delete a Categories with id
router.delete('/:id', categories.delete);

// Create a new Categories
router.delete('/', categories.deleteAll);

module.exports = router;
