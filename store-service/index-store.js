const config = require('../config');
const express = require('express');
const db = require('./models');
const cors = require('cors');
const users = require('./users/network');
const events = require('./events/network');
const categories = require('./categories/network');
const topics = require('./topics/network');
const countries = require('./countries/network');
const cities = require('./cities/network');
const modes = require('./modes/network');
const images = require('./images/network');

global.__basedir = __dirname;

const app = express();

var corsOptions = {
  origin: 'http://localhost:8081',
};
app.use(cors(corsOptions));

const Role = db.role;

function initial() {
  Role.create({
    id: 1,
    name: 'sponsor',
  });

  Role.create({
    id: 2,
    name: 'organizer',
  });

  Role.create({
    id: 3,
    name: 'admin',
  });
}
// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

db.sequelize.sync();

//If is necessary drop structure database here
/*
db.sequelize.sync({ force: true }).then(() => {
  console.log('Drop and re-sync database.');
  initial();
});
*/
// simple route
app.get('/', (req, res) => {
  res.json({ message: 'Welcome to Mysql Server.' });
});
//Routes Api Store
app.use('/api/v1/users', users);
app.use('/api/v1/events', events);
app.use('/api/v1/categories', categories);
app.use('/api/v1/topics', topics);
app.use('/api/v1/countries', countries);
app.use('/api/v1/cities', cities);
app.use('/api/v1/modes', modes);
app.use('/api/v1/images', images);
// set port, listen for requests
const PORT = process.env.PORT || config.store.PORT;

app.listen(PORT, () => {
  console.log(`Server Mysql is running on port ${PORT}.`);
});
