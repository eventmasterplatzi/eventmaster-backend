const modes = require('./mode.controller.js');
const router = require('express').Router();

// Create a new Mode
router.post('/', modes.create);

// Retrieve all Modes
router.get('/', modes.findAll);

// Retrieve all status Modes
router.get('/status', modes.findAllStatus);

// Retrieve all modes User
router.get('/user', modes.findModesUser);

// Retrieve a single Modes with id
router.get('/:id', modes.findOne);

// Update a Modes with id
router.put('/:id', modes.update);

// Delete a Modes with id
router.delete('/:id', modes.delete);

// Create a new Modes
router.delete('/', modes.deleteAll);

module.exports = router;
