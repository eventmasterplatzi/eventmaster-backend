const db = require('../models');
const Mode = db.modes;
const { Op } = db.Sequelize;

// Create and Save a new Mode
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title || !req.body.benefits || !req.body.ammount) {
    res.status(400).send({
      message: 'Content can not be empty!',
    });
    return;
  }
  // Create a Mode
  const mode = {
    title: req.body.title,
    benefits: req.body.benefits,
    ammount: req.body.ammount,
    status: req.body.status ? req.body.status : true,
    type: req.body.type ? req.body.type : 'general',
    userId: req.body.userId ? req.body.userId : 1,
  };

  // Save Mode in the database
  Mode.create(mode)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the mode.',
      });
    });
};

// Retrieve all Modes from the database.
exports.findAll = (req, res) => {
  const { title } = req.query;
  const condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Mode.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving modes.',
      });
    });
};

// Find a single Mode with an id
exports.findOne = (req, res) => {
  const { id } = req.params;

  Mode.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error retrieving mode with id=${id}`,
      });
    });
};

// Update a Mode by the id in the request
exports.update = (req, res) => {
  const { id } = req.params;

  Mode.update(req.body, {
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'Mode was updated successfully.',
        });
      } else {
        res.send({
          message: `Cannot update mode with id=${id}. Maybe mode was not found or is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error updating mode with id=${id}`,
      });
    });
};

// Delete a Mode with the specified id in the request
exports.delete = (req, res) => {
  const { id } = req.params;

  Mode.destroy({
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'Mode was deleted successfully!',
        });
      } else {
        res.send({
          message: `Cannot delete mode with id=${id}. Maybe mode was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Could not delete mode with id=${id}`,
      });
    });
};

// Delete all Modes from the database.
exports.deleteAll = (req, res) => {
  Mode.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} modes were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while removing all modes.',
      });
    });
};

// Find all Modes status
exports.findAllStatus = (req, res) => {
  Mode.findAll({ where: { status: true } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving modes.',
      });
    });
};

//Retrieve modes per user
exports.findModesUser = (req, res) => {
  const { userId } = req.query;
  const condition = userId ? { userId: `${userId}` } : null;
  Mode.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving cities.',
      });
    });
};
