module.exports = (sequelize, Sequelize) => {
  const EventCategories = sequelize.define('event_categories', {
    categoryId: {
      type: Sequelize.INTEGER,
    },
    eventId: {
      type: Sequelize.INTEGER,
    },
  });

  return EventCategories;
};
