module.exports = (sequelize, Sequelize) => {
  const Mode = sequelize.define('mode', {
    title: {
      type: Sequelize.STRING,
    },
    ammount: {
      type: Sequelize.DECIMAL,
    },
    benefits: {
      type: Sequelize.TEXT,
    },
    type: {
      type: Sequelize.ENUM({
        values: ['general', 'owner'],
      }),
      comment: 'general,owner',
    },
    status: {
      type: Sequelize.BOOLEAN,
      comment: 'ACTIVE, INACTIVE',
    },
  });

  return Mode;
};
