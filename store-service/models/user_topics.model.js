module.exports = (sequelize, Sequelize) => {
  const UserTopics = sequelize.define('user_topics', {
    topicId: {
      type: Sequelize.INTEGER,
    },
    userId: {
      type: Sequelize.INTEGER,
    },
  });

  return UserTopics;
};
