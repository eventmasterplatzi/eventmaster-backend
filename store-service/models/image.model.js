module.exports = (sequelize, DataTypes) => {
  const Image = sequelize.define('image', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    type: {
      type: DataTypes.STRING,
    },
    userId: {
      type: DataTypes.STRING,
    },
    category: {
      type: DataTypes.ENUM({
        values: ['event', 'profile-organizer', 'profile-sponsor'],
      }),
      defaultValue: 'event',
    },
    name: {
      type: DataTypes.STRING,
    },
    data: {
      type: DataTypes.BLOB('long'),
    },
  });

  return Image;
};
