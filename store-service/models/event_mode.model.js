module.exports = (sequelize, Sequelize) => {
  const EventMode = sequelize.define('event_modes', {
    quantity: {
      type: Sequelize.INTEGER,
      defaultValue: 1,
    },
    quantityUsed: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
    },
    status: {
      type: Sequelize.ENUM({ values: ['Pending', 'Parcial', 'Complete'] }),
      comment: 'pending,parcial,complete',
      defaultValue: 'Pending',
    },
  });

  return EventMode;
};
