module.exports = (sequelize, Sequelize) => {
  const EventTopics = sequelize.define('event_topics', {
    topicId: {
      type: Sequelize.INTEGER,
    },
    eventId: {
      type: Sequelize.INTEGER,
    },
  });

  return EventTopics;
};
