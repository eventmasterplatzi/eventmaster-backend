module.exports = (sequelize, Sequelize) => {
  const UserCategories = sequelize.define('user_categories', {
    categoryId: {
      type: Sequelize.INTEGER,
    },
    userId: {
      type: Sequelize.INTEGER,
    },
  });

  return UserCategories;
};
