const db = require('../models');

const Event = db.events;
const Categories = db.categories;
const Topics = db.topics;
const Modes = db.modes;
const Images = db.images;
const { Op } = db.Sequelize;
const EventModes = db.event_modes;
const EventCategories = db.event_categories;
const EventTopics = db.event_topics;
// Create and Save a new Event
exports.create = (req, res) => {
  // Validate request

  if (
    !req.body.title ||
    !req.body.description ||
    !req.body.location ||
    !req.body.dateStart
  ) {
    res.status(400).send({
      message: 'Content can not be empty!',
    });
    return;
  }

  // Create a Event
  const event = {
    title: req.body.title,
    description: req.body.description,
    location: req.body.location,
    dateStart: req.body.dateStart,
    dateEnd: req.body.dateEnd ? req.body.dateEnd : null,
    email: req.body.email ? req.body.email : null,
    url: req.body.url ? req.body.url : null,
    event_time: req.body.event_time ? req.body.event_time : '00:00:00',
    attendees: req.body.attendees ? req.body.attendees : 0,
    image: req.body.image
      ? req.body.image
      : '07914edf-0c99-4e46-b3d9-f6a349f1acdd',
    status: req.body.status ? req.body.status : 'created',
    userId: req.body.userId,
  };

  // Save Event in the database
  Event.create(event)
    .then((data) => {
      //Register categories of an event
      if (req.body.categories) {
        Categories.findAll({
          where: {
            id: {
              [Op.or]: req.body.categories,
            },
          },
        }).then((categories) => {
          data.setCategories(categories).then(() => {
            //res.send({ message: 'User was registered successfully!' });
          });
        });
      } else {
        // categories event = 1
        data.setCategories([1]).then(() => {
          //res.send({ message: 'User was registered successfully!' });
        });
      }
      //Register topics of an event
      if (req.body.topics) {
        Topics.findAll({
          where: {
            id: {
              [Op.or]: req.body.topics,
            },
          },
        }).then((topics) => {
          data.setTopics(topics).then(() => {
            //res.send({ message: 'User was registered successfully!' });
          });
        });
      } else {
        // event topic = 1
        data.setTopics([1]).then(() => {
          //res.send({ message: 'User was registered successfully!' });
        });
      }

      //Register mdoes of an event
      if (req.body.modes) {
        Modes.findAll({
          where: {
            id: {
              [Op.or]: req.body.modes,
            },
          },
        }).then((modes) => {
          data.setModes(modes).then(() => {
            //res.send({ message: 'User was registered successfully!' });
          });
        });
      } else {
        // event mode = 1
        data.setModes([1]).then(() => {
          //res.send({ message: 'User was registered successfully!' });
        });
      }
      //end
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err || 'Some error occurred while creating the event.',
      });
    });
};

// Retrieve all Events from the database.
exports.findAll = (req, res) => {
  const { title } = req.query;
  const condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Event.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving events.',
      });
    });
};
//event per users

// Find a single Event Id
exports.findOne = (req, res) => {
  const { id } = req.params;

  Event.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error retrieving event with id=${id}`,
      });
    });
};

// Find a single Event Id with path image
exports.findOne = (req, res) => {
  const { id } = req.params;
  var obj_data_event, obj_json;
  var eventmodes = [];
  var categories = [];
  var topics = [];

  //Get event categories
  db.sequelize
    .query('SELECT * FROM event_categories where eventId=' + id, null, {
      raw: true,
      plain: false,
    })
    .then(function (category) {
      for (let j = 0; j < category[0].length; j++) {
        categories.push(category[0][j].categoryId);
      }
    });
  //Get event topics
  db.sequelize
    .query('SELECT * FROM event_topics where eventId=' + id, null, {
      raw: true,
      plain: false,
    })
    .then(function (topic) {
      for (let h = 0; h < topic[0].length; h++) {
        topics.push(topic[0][h].topicId);
      }
    });
  //Get event modes
  db.sequelize
    .query('SELECT * FROM event_modes where eventId=' + id, null, {
      raw: true,
      plain: false,
    })
    .then(function (mode) {
      for (let k = 0; k < mode[0].length; k++) {
        eventmodes.push(mode[0][k].modeId);
      }
    });
  Event.findByPk(id)
    .then((data) => {
      //access Event Image
      obj_data_event = data;

      //Get event image
      Images.findAll({ where: { id: data.image } })
        .then((dataimage) => {
          obj_data_event = JSON.stringify(obj_data_event);
          obj_json = JSON.parse(obj_data_event);
          obj_json.path_image = dataimage[0].dataValues.name;
          obj_json.type_image = dataimage[0].dataValues.type;
          obj_json.category_image = dataimage[0].dataValues.category;
          obj_json.categories = categories;
          obj_json.topics = topics;
          obj_json.modes = eventmodes;
          res.status(200).send(obj_json);
        })
        .catch((err) => {
          res.status(500).send({
            message:
              err.message || 'Some error occurred while retrieving images.',
          });
        });
      //end image Event
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error retrieving event with id=${id}` + err,
      });
    });
};

// Find a single Event with an status
exports.findOneStatus = (req, res) => {
  const { status } = req.body;
  Event.findAll({ where: { status: status } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving events.',
      });
    });
};

// Update a Event by the id in the request
exports.update = (req, res) => {
  const { id } = req.params;
  var categories = [];
  var topics = [];
  var modes = [];
  var obj_data_user,
    obj_json = [];
  Event.update(req.body, {
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        if (req.body.categories) {
          for (let i = 0; i < req.body.categories.length; i++) {
            EventCategories.findOrCreate({
              where: {
                categoryId: req.body.categories[i],
                eventId: id,
              },
            }).spread(function (cat, created) {
              if (created) {
                console.log('New category add ' + req.body.categories[i]);
              } else {
                //Register event preferred categories
                if (!req.body.categories.includes(req.body.categories[i])) {
                  db.sequelize
                    .query(
                      'DELETE FROM event_categories where eventId=' +
                        id +
                        ' AND categoryId=' +
                        req.body.categories[i],
                      null,
                      {
                        raw: true,
                      }
                    )
                    .then(function (category) {
                      console.log('Deleted category' + req.body.categories[i]);
                    });
                } else {
                  console.log(
                    'This Category ' + req.body.categories[i] + ' stay here'
                  );
                }
              }
            });
          }
          EventCategories.destroy({
            where: {
              categoryId: { [Op.notIn]: req.body.categories },
              eventId: id,
            },
          }).then((num) => {
            if (num == 1) {
              console.log('Some categories was deleted successfully!');
            } else {
              console.log('Any category was deleted!');
            }
          });
        }
        //Register event preferred topics
        if (req.body.topics) {
          for (let i = 0; i < req.body.topics.length; i++) {
            EventTopics.findOrCreate({
              where: {
                topicId: req.body.topics[i],
                eventId: id,
              },
            }).spread(function (top, created) {
              if (created) {
                console.log('New topic add ' + req.body.topics[i]);
              } else {
                //Get user topics
                if (!req.body.topics.includes(req.body.topics[i])) {
                  db.sequelize
                    .query(
                      'DELETE FROM event_topics where eventId=' +
                        id +
                        ' AND topicId=' +
                        req.body.topics[i],
                      null,
                      {
                        raw: true,
                      }
                    )
                    .then(function (topic) {
                      console.log('Deleted topic' + req.body.topics[i]);
                    });
                } else {
                  console.log(
                    'This Topic ' + req.body.topics[i] + ' stay here'
                  );
                }
              }
            });
          }
          EventTopics.destroy({
            where: {
              topicId: { [Op.notIn]: req.body.topics },
              eventId: id,
            },
          }).then((num) => {
            if (num == 1) {
              console.log('Some topics was deleted successfully!');
            } else {
              console.log('Any topic was deleted!');
            }
          });
        }
        //Register event preferred modes
        if (req.body.modes) {
          for (let i = 0; i < req.body.modes.length; i++) {
            EventModes.findOrCreate({
              where: {
                modeId: req.body.modes[i],
                eventId: id,
              },
            }).spread(function (mod, created) {
              if (created) {
                console.log('New mode add ' + req.body.modes[i]);
              } else {
                //Get user topics
                if (!req.body.modes.includes(req.body.modes[i])) {
                  db.sequelize
                    .query(
                      'DELETE FROM event_modes where eventId=' +
                        id +
                        ' AND modeId=' +
                        req.body.modes[i],
                      null,
                      {
                        raw: true,
                      }
                    )
                    .then(function (topic) {
                      console.log('Deleted mode' + req.body.modes[i]);
                    });
                } else {
                  console.log('This Mode ' + req.body.modes[i] + ' stay here');
                }
              }
            });
          }
          EventModes.destroy({
            where: {
              modeId: { [Op.notIn]: req.body.modes },
              eventId: id,
            },
          }).then((num) => {
            if (num == 1) {
              console.log('Some modes was deleted successfully!');
            } else {
              console.log('Any modes was deleted!');
            }
          });
        }
        res.send({
          message: 'Event was updated successfully.',
        });
      } else {
        res.send({
          message: `Cannot update event with id=${id}. Maybe event was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error updating event with id=${id}`,
      });
    });
};

// Delete a Event with the specified id in the request
exports.delete = (req, res) => {
  const { id } = req.params;

  Event.destroy({
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'Event was deleted successfully!',
        });
      } else {
        res.send({
          message: `Cannot delete event with id=${id}. Maybe event was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Could not delete event with id=${id}`,
      });
    });
};

// Delete all Events from the database.
exports.deleteAll = (req, res) => {
  Event.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} events were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while removing all events.',
      });
    });
};

// Find all published Events
exports.findAllPublished = (req, res) => {
  Event.findAll({ where: { status: 'published' } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving events.',
      });
    });
};
